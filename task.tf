resource "aws_ecs_task_definition" "task1" {
  family = "copy-mcare-score-admin-api"
  cpu = 256
  memory = 512
  task_role_arn = "arn:aws:iam::296974131446:role/ecsTaskExecutionRole"
  execution_role_arn = "arn:aws:iam::296974131446:role/ecsTaskExecutionRole"
  network_mode = "bridge"

  depends_on = [aws_ecr_repository.ecr]

  container_definitions = jsonencode([
    {
      "dnsSearchDomains": null,
      "environmentFiles": null,
      "logConfiguration": {
        "logDriver": "awslogs",
        "secretOptions": null,
        "options": {
          "awslogs-group": "/ecs/mcare-score-admin-api",
          "awslogs-region": "us-west-2",
          "awslogs-stream-prefix": "ecs"
        }
      },
      "entryPoint": null,
      "portMappings": [
        {
          "hostPort": 0,
          "protocol": "tcp",
          "containerPort": 5000
        }
      ],
      "command": null,
      "linuxParameters": null,
      "cpu": 0,
      "environment": [
        {
          "name": "ATHENA_DB",
          "value": "scorebus"
        },
        {
          "name": "ATHENA_RESULTS_LOCATION",
          "value": "s3://artemis-hml-athena-query-result/score-results"
        },
        {
          "name": "AUTH0_AUDIENCE",
          "value": "https://score-admin-api.oi.com.br/"
        },
        {
          "name": "AUTH0_ISSUER_URL",
          "value": "https://mobicare.auth0.com/"
        },
        {
          "name": "AUTH0_JWKS_URL",
          "value": "https://mobicare.auth0.com/"
        },
        {
          "name": "AWS_REGION",
          "value": "us-west-2"
        },
        {
          "name": "DB_DATABASE",
          "value": "scorebus"
        },
        {
          "name": "DB_HOST",
          "value": "artemis-h-mysql-01.mobicare.com.br"
        },
        {
          "name": "DB_PASSWORD",
          "value": "oijIUSBfiuQ&T54"
        },
        {
          "name": "DB_PORT",
          "value": "3306"
        },
        {
          "name": "DB_USERNAME",
          "value": "sys-artemis"
        },
        {
          "name": "ELASTICSEARCH_URL",
          "value": "http://elasticsearch-artemis-hml.oiads.com.br:9200"
        },
        {
          "name": "LOG_LEVEL",
          "value": "debug"
        },
        {
          "name": "REPORT_LOGS_INDEX",
          "value": "kong-scorebus-*"
        },
        {
          "name": "SCORE_LOGS_S3",
          "value": "s3://artemis-hml-logs.mobicare.com.br/kong-logstash"
        }
      ],
      "resourceRequirements": null,
      "ulimits": null,
      "dnsServers": null,
      "mountPoints": [],
      "workingDirectory": null,
      "secrets": null,
      "dockerSecurityOptions": null,
      "memory": null,
      "memoryReservation": null,
      "volumesFrom": [],
      "stopTimeout": null,
      "image": "296974131446.dkr.ecr.us-west-2.amazonaws.com/mcare-score-admin-api:81a9179fdcdc4796175450a518ad146ad30de041",
      "startTimeout": null,
      "firelensConfiguration": null,
      "dependsOn": null,
      "disableNetworking": null,
      "interactive": null,
      "healthCheck": null,
      "essential": true,
      "links": null,
      "hostname": null,
      "extraHosts": null,
      "pseudoTerminal": null,
      "user": null,
      "readonlyRootFilesystem": null,
      "dockerLabels": null,
      "systemControls": null,
      "privileged": null,
      "name": "copy-mcare-score-admin-api"
    }
  ])
}

resource "aws_ecs_task_definition" "task2" {
  family = "copy-mcare-score-oi"
  cpu = 256
  memory = 512
  task_role_arn = "arn:aws:iam::296974131446:role/ecsTaskExecutionRole"
  execution_role_arn = "arn:aws:iam::296974131446:role/ecsTaskExecutionRole"
  network_mode = "bridge"

  depends_on = [aws_ecr_repository.ecr2]

  container_definitions = jsonencode([
    {
      "dnsSearchDomains": null,
      "environmentFiles": null,
      "logConfiguration": {
        "logDriver": "json-file",
        "secretOptions": null,
        "options": {
          "max-size": "100m",
          "max-file": "3"
        }
      },
      "entryPoint": null,
      "portMappings": [
        {
          "hostPort": 0,
          "protocol": "tcp",
          "containerPort": 5005
        }
      ],
      "command": null,
      "linuxParameters": null,
      "cpu": 0,
      "environment": [
        {
          "name": "auth0.audience",
          "value": "https://score-api.oi.com.br/"
        },
        {
          "name": "aws.dynamodb.region",
          "value": "us-west-2"
        },
        {
          "name": "cpf.completezeros",
          "value": "true"
        },
        {
          "name": "server.port",
          "value": "5005"
        },
        {
          "name": "server.servlet.contextPath",
          "value": "/mcare-score"
        },
        {
          "name": "spring.security.oauth2.resourceserver.jwt.issuer-uri",
          "value": "https://mobicare.auth0.com/"
        }
      ],
      "resourceRequirements": null,
      "ulimits": null,
      "dnsServers": null,
      "mountPoints": [],
      "workingDirectory": null,
      "secrets": null,
      "dockerSecurityOptions": null,
      "memory": null,
      "memoryReservation": null,
      "volumesFrom": [],
      "stopTimeout": null,
      "image": "296974131446.dkr.ecr.us-west-2.amazonaws.com/mcare-score-oi:d984497a98fba38bae083e55e72cdff936b4be25",
      "startTimeout": null,
      "firelensConfiguration": null,
      "dependsOn": null,
      "disableNetworking": null,
      "interactive": null,
      "healthCheck": null,
      "essential": true,
      "links": null,
      "hostname": null,
      "extraHosts": null,
      "pseudoTerminal": null,
      "user": null,
      "readonlyRootFilesystem": null,
      "dockerLabels": null,
      "systemControls": null,
      "privileged": null,
      "name": "copy-mcare-score-oi"
    }
  ])
}