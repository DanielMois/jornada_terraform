resource "aws_codebuild_project" "codebuild1" {
  name           = "copy-mcare-score-admin-api"
  build_timeout  = "15"
  service_role   = "arn:aws:iam::296974131446:role/service-role/codebuild-copy-mcare-score-admin-api-service-role"
  encryption_key = "arn:aws:kms:us-west-2:296974131446:alias/aws/s3"


  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:3.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true

    environment_variable {
      name  = "AWS_REGION"
      value = "us-west-2"
    }

    environment_variable {
      name  = "PROJECT_NAME"
      value = "copy-mcare-score-admin-api"
    }

    environment_variable {
      name  = "CLUSTER_NAME"
      value = "mcare-artemis-hml"
    }

    environment_variable {
      name  = "SERVICE_NAME"
      value = "copy-mcare-score-admin-api"
    }

    environment_variable {
      name  = "AWS_ACCOUNT_ID"
      value = "296974131446"
    }

    environment_variable {
      name  = "DEPLOY_BUCKET"
      value = "artemis-hml-deployments"
    }

    environment_variable {
      name  = "NETWORKMODE"
      value = "bridge"
    }

  }

  source {
    type            = "BITBUCKET"
    location        = "https://fgoldin@bitbucket.org/mobicare-git/mcare-score-admin-api.git"
    git_clone_depth = 1
  }

  source_version = "homolog"

  tags = {
    MOBI_COMPANY     = "${var.tags["MOBI_COMPANY"]}"
    MOBI_COST_CENTER = "${var.tags["MOBI_COST_CENTER"]}"
    MOBI_CUSTOMER    = "${var.tags["MOBI_CUSTOMER"]}"
    MOBI_PROJECT     = "${var.tags["MOBI_PROJECT"]}"
    MOBI_STACK       = "${var.tags["MOBI_STACK"]}"
    MOBI_NAME        = "copy-mcare-score-admin-api"
  }

  depends_on = [aws_iam_role.codebuild-iam1, aws_iam_role_policy.resource, aws_iam_role_policy.resourcedois, aws_iam_role_policy.resourcetres]
}



resource "aws_codebuild_webhook" "webhook1" {
  project_name = "copy-mcare-score-admin-api"

  filter_group {
    filter {
      type    = "EVENT"
      pattern = "PULL_REQUEST_MERGED"
    }
  }

  depends_on = [aws_codebuild_project.codebuild1]
}

resource "aws_iam_role" "codebuild-iam1" {
  name = "codebuild-copy-mcare-score-admin-api-service-role"

  path = "/service-role/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "resource" {
  role = "codebuild-copy-mcare-score-admin-api-service-role"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Resource": [
                "*"
            ],
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ]
        },
        {
            "Effect": "Allow",
            "Resource": [
                "*"
            ],
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:GetBucketAcl",
                "s3:GetBucketLocation"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "codebuild:CreateReportGroup",
                "codebuild:CreateReport",
                "codebuild:UpdateReport",
                "codebuild:BatchPutTestCases",
                "codebuild:BatchPutCodeCoverages"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
POLICY

  depends_on = [aws_iam_role.codebuild-iam1, aws_iam_role.codebuild-iam2]
}
resource "aws_iam_role_policy" "resourcedois" {
  role   = "codebuild-copy-mcare-score-admin-api-service-role"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "application-autoscaling:DeleteScalingPolicy",
                "application-autoscaling:DeregisterScalableTarget",
                "application-autoscaling:DescribeScalableTargets",
                "application-autoscaling:DescribeScalingActivities",
                "application-autoscaling:DescribeScalingPolicies",
                "application-autoscaling:PutScalingPolicy",
                "application-autoscaling:RegisterScalableTarget",
                "appmesh:DescribeVirtualGateway",
                "appmesh:DescribeVirtualNode",
                "appmesh:ListMeshes",
                "appmesh:ListVirtualGateways",
                "appmesh:ListVirtualNodes",
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:DeleteAutoScalingGroup",
                "autoscaling:DeleteLaunchConfiguration",
                "autoscaling:Describe*",
                "autoscaling:UpdateAutoScalingGroup",
                "cloudformation:CreateStack",
                "cloudformation:DeleteStack",
                "cloudformation:DescribeStack*",
                "cloudformation:UpdateStack",
                "cloudwatch:DeleteAlarms",
                "cloudwatch:DescribeAlarms",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:PutMetricAlarm",
                "codedeploy:BatchGetApplicationRevisions",
                "codedeploy:BatchGetApplications",
                "codedeploy:BatchGetDeploymentGroups",
                "codedeploy:BatchGetDeployments",
                "codedeploy:ContinueDeployment",
                "codedeploy:CreateApplication",
                "codedeploy:CreateDeployment",
                "codedeploy:CreateDeploymentGroup",
                "codedeploy:GetApplication",
                "codedeploy:GetApplicationRevision",
                "codedeploy:GetDeployment",
                "codedeploy:GetDeploymentConfig",
                "codedeploy:GetDeploymentGroup",
                "codedeploy:GetDeploymentTarget",
                "codedeploy:ListApplicationRevisions",
                "codedeploy:ListApplications",
                "codedeploy:ListDeploymentConfigs",
                "codedeploy:ListDeploymentGroups",
                "codedeploy:ListDeployments",
                "codedeploy:ListDeploymentTargets",
                "codedeploy:RegisterApplicationRevision",
                "codedeploy:StopDeployment",
                "ec2:AssociateRouteTable",
                "ec2:AttachInternetGateway",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CancelSpotFleetRequests",
                "ec2:CreateInternetGateway",
                "ec2:CreateLaunchTemplate",
                "ec2:CreateRoute",
                "ec2:CreateRouteTable",
                "ec2:CreateSecurityGroup",
                "ec2:CreateSubnet",
                "ec2:CreateVpc",
                "ec2:DeleteLaunchTemplate",
                "ec2:DeleteSubnet",
                "ec2:DeleteVpc",
                "ec2:Describe*",
                "ec2:DetachInternetGateway",
                "ec2:DisassociateRouteTable",
                "ec2:ModifySubnetAttribute",
                "ec2:ModifyVpcAttribute",
                "ec2:RequestSpotFleet",
                "ec2:RunInstances",
                "ecs:*",
                "elasticfilesystem:DescribeAccessPoints",
                "elasticfilesystem:DescribeFileSystems",
                "elasticloadbalancing:CreateListener",
                "elasticloadbalancing:CreateLoadBalancer",
                "elasticloadbalancing:CreateRule",
                "elasticloadbalancing:CreateTargetGroup",
                "elasticloadbalancing:DeleteListener",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:DeleteRule",
                "elasticloadbalancing:DeleteTargetGroup",
                "elasticloadbalancing:DescribeListeners",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:DescribeRules",
                "elasticloadbalancing:DescribeTargetGroups",
                "events:DeleteRule",
                "events:DescribeRule",
                "events:ListRuleNamesByTarget",
                "events:ListTargetsByRule",
                "events:PutRule",
                "events:PutTargets",
                "events:RemoveTargets",
                "fsx:DescribeFileSystems",
                "iam:ListAttachedRolePolicies",
                "iam:ListInstanceProfiles",
                "iam:ListRoles",
                "lambda:ListFunctions",
                "logs:CreateLogGroup",
                "logs:DescribeLogGroups",
                "logs:FilterLogEvents",
                "route53:CreateHostedZone",
                "route53:DeleteHostedZone",
                "route53:GetHealthCheck",
                "route53:GetHostedZone",
                "route53:ListHostedZonesByName",
                "servicediscovery:CreatePrivateDnsNamespace",
                "servicediscovery:CreateService",
                "servicediscovery:DeleteService",
                "servicediscovery:GetNamespace",
                "servicediscovery:GetOperation",
                "servicediscovery:GetService",
                "servicediscovery:ListNamespaces",
                "servicediscovery:ListServices",
                "servicediscovery:UpdateService",
                "sns:ListTopics"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:GetParametersByPath"
            ],
            "Resource": "arn:aws:ssm:*:*:parameter/aws/service/ecs*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DeleteInternetGateway",
                "ec2:DeleteRoute",
                "ec2:DeleteRouteTable",
                "ec2:DeleteSecurityGroup"
            ],
            "Resource": [
                "*"
            ],
            "Condition": {
                "StringLike": {
                    "ec2:ResourceTag/aws:cloudformation:stack-name": "EC2ContainerService-*"
                }
            }
        },
        {
            "Action": "iam:PassRole",
            "Effect": "Allow",
            "Resource": [
                "*"
            ],
            "Condition": {
                "StringLike": {
                    "iam:PassedToService": "ecs-tasks.amazonaws.com"
                }
            }
        },
        {
            "Action": "iam:PassRole",
            "Effect": "Allow",
            "Resource": [
                "arn:aws:iam::*:role/ecsInstanceRole*"
            ],
            "Condition": {
                "StringLike": {
                    "iam:PassedToService": [
                        "ec2.amazonaws.com",
                        "ec2.amazonaws.com.cn"
                    ]
                }
            }
        },
        {
            "Action": "iam:PassRole",
            "Effect": "Allow",
            "Resource": [
                "arn:aws:iam::*:role/ecsAutoscaleRole*"
            ],
            "Condition": {
                "StringLike": {
                    "iam:PassedToService": [
                        "application-autoscaling.amazonaws.com",
                        "application-autoscaling.amazonaws.com.cn"
                    ]
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "*",
            "Condition": {
                "StringLike": {
                    "iam:AWSServiceName": [
                        "autoscaling.amazonaws.com",
                        "ecs.amazonaws.com",
                        "ecs.application-autoscaling.amazonaws.com",
                        "spot.amazonaws.com",
                        "spotfleet.amazonaws.com"
                    ]
                }
            }
        }
    ]
}
POLICY

  depends_on = [aws_iam_role.codebuild-iam1]
}

resource "aws_iam_role_policy" "resourcetres" {
  role   = "codebuild-copy-mcare-score-admin-api-service-role"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "imagebuilder:GetComponent",
                "imagebuilder:GetContainerRecipe",
                "ecr:GetAuthorizationToken",
                "ecr:BatchGetImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:PutImage"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": "*",
            "Condition": {
                "ForAnyValue:StringEquals": {
                    "kms:EncryptionContextKeys": "aws:imagebuilder:arn",
                    "aws:CalledVia": [
                        "imagebuilder.amazonaws.com"
                    ]
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": "arn:aws:s3:::ec2imagebuilder*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:CreateLogGroup",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:log-group:/aws/imagebuilder/*"
        }
    ]
}
POLICY

  depends_on = [aws_iam_role.codebuild-iam1, aws_iam_role.codebuild-iam2]
}

resource "aws_codebuild_project" "codebuild2" {
  name           = "copy-mcare-score-oi"
  service_role   = "arn:aws:iam::296974131446:role/service-role/codebuild-mcare-score-oi-service-role"
  encryption_key = "arn:aws:kms:us-west-2:296974131446:alias/aws/s3"


  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:3.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = true

    environment_variable {
      name  = "AWS_REGION"
      value = "us-west-2"
    }

    environment_variable {
      name  = "PROJECT_NAME"
      value = "copy-mcare-score-oi"
    }

    environment_variable {
      name  = "CLUSTER_NAME"
      value = "mcare-artemis-hml"
    }

    environment_variable {
      name  = "SERVICE_NAME"
      value = "copy-mcare-score-oi"
    }

    environment_variable {
      name  = "AWS_ACCOUNT_ID"
      value = "296974131446"
    }

    environment_variable {
      name  = "DEPLOY_BUCKET"
      value = "artemis-hml-deployments"
    }

    environment_variable {
      name  = "NETWORKMODE"
      value = "bridge"
    }

  }

  source {
    type            = "BITBUCKET"
    location        = "https://fgoldin@bitbucket.org/mobicare-git/mcare-score-oi.git"
    git_clone_depth = 1
  }

  source_version = "homolog"

  tags = {
    MOBI_COMPANY     = "${var.tags["MOBI_COMPANY"]}"
    MOBI_COST_CENTER = "OPR-WIFI"
    MOBI_CUSTOMER    = "${var.tags["MOBI_CUSTOMER"]}"
    MOBI_PROJECT     = "${var.tags["MOBI_PROJECT"]}"
    MOBI_STACK       = "${var.tags["MOBI_STACK"]}"
    MOBI_NAME        = "copy-mcare-score-oi"
  }

  depends_on = [aws_iam_role.codebuild-iam2, aws_iam_role_policy.resource, aws_iam_role_policy.resourcequatro, aws_iam_role_policy.resourcetres]
}

resource "aws_codebuild_webhook" "webhook2" {
  project_name = "copy-mcare-score-oi"

  filter_group {
    filter {
      type    = "EVENT"
      pattern = "PULL_REQUEST_MERGED"
    }
  }

  depends_on = [aws_codebuild_project.codebuild2]
}

resource "aws_iam_role" "codebuild-iam2" {
  name = "codebuild-copy-mcare-score-oi-service-role"

  path = "/service-role/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "resourcequatro" {
  role   = "codebuild-copy-mcare-score-oi-service-role"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Resource": [
                "arn:aws:logs:us-west-2:296974131446:log-group:/aws/codebuild/mcare-score-oi",
                "arn:aws:logs:us-west-2:296974131446:log-group:/aws/codebuild/mcare-score-oi:*"
            ],
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ]
        },
        {
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::codepipeline-us-west-2-*"
            ],
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:GetBucketAcl",
                "s3:GetBucketLocation"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "codebuild:CreateReportGroup",
                "codebuild:CreateReport",
                "codebuild:UpdateReport",
                "codebuild:BatchPutTestCases",
                "codebuild:BatchPutCodeCoverages"
            ],
            "Resource": [
                "arn:aws:codebuild:us-west-2:296974131446:report-group/mcare-score-oi-*"
            ]
        }
    ]
}
POLICY

  depends_on = [aws_iam_role.codebuild-iam2]
}