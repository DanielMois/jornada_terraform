resource "aws_ecr_repository" "ecr" {
  name                 = "copy-mcare-score-admin-api"
  image_tag_mutability = "MUTABLE"
  tags                 = {
                            MOBI_COMPANY = "${var.tags["MOBI_COMPANY"]}"
                           MOBI_COST_CENTER = "${var.tags["MOBI_COST_CENTER"]}"
                            MOBI_CUSTOMER = "${var.tags["MOBI_CUSTOMER"]}"
                            MOBI_PROJECT = "${var.tags["MOBI_PROJECT"]}"
                            MOBI_STACK = "${var.tags["MOBI_STACK"]}"
                            MOBI_NAME = "copy-mcare-score-admin-api"
                        }
  
}

resource "aws_ecr_repository" "ecr2" {
  name                 = "copy-mcare-score-oi"
  image_tag_mutability = "MUTABLE"
  tags                 = {
                           MOBI_COMPANY = "${var.tags["MOBI_COMPANY"]}"
                           MOBI_COST_CENTER = "${var.tags["MOBI_COST_CENTER"]}"
                           MOBI_CUSTOMER = "${var.tags["MOBI_CUSTOMER"]}"
                           MOBI_PROJECT = "${var.tags["MOBI_PROJECT"]}"
                           MOBI_NAME = "copy-mcare-score-oi"
                        }
  
}

