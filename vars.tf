variable "tags" {
    type = map(string)
    default = {
        MOBI_COMPANY = "MOBICARE",
        MOBI_COST_CENTER = "OPR_DATA_ADS",
        MOBI_CUSTOMER = "OI_NN"
        MOBI_PROJECT = "ARTEMIS_QUOD"
        MOBI_STACK = "HOMOLOG"
    }

}