resource "aws_s3_bucket" "b" {
  bucket = "copy-score-hml-files-static.mobicare.com.br"
  acl    = "public-read"
  tags   = {
    MOBI_COMPANY = "${var.tags["MOBI_COMPANY"]}"
    MOBI_COST_CENTER = "${var.tags["MOBI_COST_CENTER"]}"
    MOBI_CUSTOMER = "${var.tags["MOBI_CUSTOMER"]}"
    MOBI_PROJECT = "${var.tags["MOBI_PROJECT"]}"
    MOBI_STACK = "${var.tags["MOBI_STACK"]}"
    MOBI_NAME = "copy-score-hml-files-static.mobicare.com.br"
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  website {
    index_document = "index.html"
  }
  force_destroy = true
  versioning {
    enabled = true
  }
}

resource "null_resource" "s3_objects" {
  provisioner "local-exec" {
    command = "aws s3 cp s3://score-hml-files-static.mobicare.com.br s3://copy-score-hml-files-static.mobicare.com.br --recursive"
  }
  depends_on = [aws_s3_bucket.b]
  
}
