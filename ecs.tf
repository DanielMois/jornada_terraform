resource "aws_ecs_service" "service" {
  name            = "copy-mcare-score-admin-api"
  cluster         = "mcare-artemis-hml"
  task_definition = aws_ecs_task_definition.task1.arn
  desired_count   = 1
  iam_role        = "arn:aws:iam::296974131446:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent = 200

  ordered_placement_strategy {
    type = "spread"
    field = "attribute:ecs.availability-zon"
  }

  ordered_placement_strategy {
    type = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "arn:aws:elasticloadbalancing:us-west-2:296974131446:targetgroup/tg-mcare-score-admin-api/22c836fdfbb1b447"
    container_name   = "copy-mcare-score-admin-api"
    container_port   = 5000
  }

  depends_on      = [aws_ecs_task_definition.task1]

}

resource "aws_ecs_service" "service2" {
  name            = "copy-mcare-score-oi"
  cluster         = "mcare-artemis-hml"
  task_definition = aws_ecs_task_definition.task2.arn
  desired_count   = 1
  iam_role        = "arn:aws:iam::296974131446:role/aws-service-role/ecs.amazonaws.com/AWSServiceRoleForECS"
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent = 200

  ordered_placement_strategy {
    type = "spread"
    field = "attribute:ecs.availability-zon"
  }

  ordered_placement_strategy {
    type = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "arn:aws:elasticloadbalancing:us-west-2:296974131446:targetgroup/tg-mcare-score-oi/f7b927054c6c4480"
    container_name   = "copy-mcare-score-oi"
    container_port   = 5005
  }

  lifecycle {
    ignore_changes = [desired_count, task_definition]
  }


}