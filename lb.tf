resource "aws_lb" "COPYEXT" {
  name               = "COPYEXT"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["sg-0b8a16cb1af762d53"]
  subnets            = ["subnet-0a825c2fbdfe466d8","subnet-0a92e98fed484529b"]

  enable_deletion_protection = false

  tags = {
    Environment = "homolog"
  }
}

resource "aws_lb_target_group" "tg1" {
  name     = "copy-tg-mcare-score-admin-api"
  port     = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id   = "vpc-027493bafb6fe63fe"
}

resource "aws_lb_target_group" "tg2" {
  name     = "copy-tg-mcare-score-oi"
  port     = 80
  protocol = "HTTP"
  target_type = "instance"
  vpc_id   = "vpc-027493bafb6fe63fe"
}

resource "aws_lb_target_group_attachment" "at1" {
  target_group_arn = aws_lb_target_group.tg1.arn
  target_id        = "i-05fd9d16dc7c2fccc"
  port             = 80

  depends_on = [aws_lb_target_group.tg1]
}

resource "aws_lb_target_group_attachment" "at2" {
  target_group_arn = aws_lb_target_group.tg2.arn
  target_id        = "i-05fd9d16dc7c2fccc"
  port             = 80

  depends_on = [aws_lb_target_group.tg2]
}
